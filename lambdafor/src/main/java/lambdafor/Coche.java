package lambdafor;

public class Coche {

	
	private String marca;
	private String matricula;
	private int nRuedas;
	
	
	
	
	
	public Coche() {
		
	}
	
	
	public Coche(String marca, String matricula, int nRuedas) {
		super();
		this.marca = marca;
		this.matricula = matricula;
		this.nRuedas = nRuedas;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public int getnRuedas() {
		return nRuedas;
	}
	public void setnRuedas(int nRuedas) {
		this.nRuedas = nRuedas;
	}


	@Override
	public String toString() {
		return "Coche [marca=" + marca + ", matricula=" + matricula + ", nRuedas=" + nRuedas + "]";
	}
	
	
	


	

}
