package lambdafor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LambdaFor {

	public static void main(String[] args) {

		//a�adimos a la lista los coches forma 1
		List<Coche> listaCoches = Arrays.asList(new Coche("Bmw", "bbb002", 4),
				new Coche("Mercedes","FFF222",4));

		//forma 2
		//	Coche coche = new Coche("Bmw", "bbb002", 4);
		//	Coche coche2 = new Coche("Mercedes","FFF222",4);

		//	listaCoches.add(coche);
		//	listaCoches.add(coche2);


		//bucle for 
		System.out.println("bucle for ");
		for(int i=0; i<listaCoches.size(); i++) {
			System.out.println(listaCoches.get(i).toString());
		}

		//foreach
		System.out.println("\n"+"bucle for each");
		for(Coche c : listaCoches) {
			System.out.println(c.toString());
		}

		//lambdaForEachConStream
		System.out.println("\n"+"bucle for each con lambda en stream");
		listaCoches.stream().forEach(c ->System.out.println(c.toString()));

		//lambdaForEachSinStream
		System.out.println("\n"+"bucle for each con lambda sin stream");
		listaCoches.forEach(c ->System.out.println(c.toString()));
	}

}
